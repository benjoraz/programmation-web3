import {createRouter, createWebHistory} from 'vue-router';

import PageOne from '../components/PageOne.vue'
import PageTwo from '../components/PageTwo.vue'
import notFound from '../components/notFound.vue'

const routes = [{
    path: '/',
    name: 'home',
    component: PageOne
    },

    {
    path: '/:id(\\d+)',
    name: 'game',
    component: PageTwo
    },

    {
    path: '/:catchAll(.*)',
    name: 'notfound',
    component: notFound
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior() {
        return { top: 0 }
      },
})

export default router